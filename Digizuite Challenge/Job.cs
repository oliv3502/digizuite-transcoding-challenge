﻿namespace Digizuite_Challenge
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A transcoding job
    /// </summary>
    public class Job
    {
        public int jobID {get; private set;}
        public string jobName {get; private set;}
        public int[] jobDependencies {get; private set;}

        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class
        /// </summary>
        /// <param name="jobID">the id of the job. Cannot be 0</param>
        /// <param name="jobName">the name of the job</param>
        /// <param name="jobDependencies">the jobs that have to be executed before this job</param>
        public Job(int jobID, string jobName, int[] jobDependencies)
        {
            this.jobID = jobID == 0 ? throw new ArgumentOutOfRangeException("jobÌD can't be 0") : jobID;
            this.jobName = jobName;
            this.jobDependencies = jobDependencies.Length == 1 && jobDependencies[0] == 0 ? new int[0] : jobDependencies;
        }

        /// <summary>
        /// Sorts an array of jobs in a Dictionary grouped by each "tier" of jobs that can be run based on their dependencies
        /// </summary>
        /// <param name="jobs"> the array of jobs to be sorted</param>
        /// <returns>a Dictionary with each group, starting at 1, as the key and an array of job as the value</returns>
        public static Dictionary<int, Job[]> SortJobsInGroups(Job[] jobs)
        {
            List<Job> remainingJobs = new List<Job>(jobs);
            List<Job> addedJobs = new List<Job>();
            Dictionary<int, Job[]> groupedJobs = new Dictionary<int, Job[]>();
            int groupIndex = 1;

            while (remainingJobs.LongCount() != 0)
            {
                List<Job> jobsToBeAdded = new List<Job>();
                foreach (Job job in remainingJobs)
                {
                    
                    bool hasDependencies = true;
                    foreach (int dependency in job.jobDependencies)
                    {
                        if (addedJobs.Find(addedJob => addedJob.jobID == dependency) == null)
                        {
                            hasDependencies = false;
                        }
                    }
                    if (hasDependencies)
                    {
                        jobsToBeAdded.Add(job);
                    }
                }
                foreach (Job job in jobsToBeAdded)
                {
                    addedJobs.Add(job);
                    remainingJobs.Remove(job);
                }
                groupedJobs[groupIndex] = jobsToBeAdded.ToArray();
                groupIndex++;
            }

            return groupedJobs;
        }
    }
}
