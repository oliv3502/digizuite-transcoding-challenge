﻿namespace Digizuite_Challenge
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A program to load jobs from a file and group them by dependencies
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The jobs to be sorted by the program
        /// </summary>
        private Job[] jobs;

        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class and call the <see cref="getJobs"/> method
        /// </summary>
        public Program()
        {
            getJobs("./jobs.csv");
        }

        /// <summary>
        /// Populates the <see cref="jobs"/> array with data from a local file.
        /// The file expected is a csv in the format of: 
        /// <example>
        /// <code>
        /// jobID, jobName, jobDependency1; jobDependency2; ...; jobDependencyN
        /// </code>
        /// </example>
        /// A job without dependencies is just a single dependency with 0
        /// <param name="path">the path to the input file</param>
        /// </summary>
        private void getJobs(string path)
        {
            string[] csvLines = File.ReadAllLines(path);
            jobs = new Job[csvLines.Length];
            for (int i = 0; i < csvLines.Length; i++)
            {
                string[] jobArguments = csvLines[i].Split(',');
                jobs[i] = new Job(int.Parse(jobArguments[0]), jobArguments[1], Array.ConvertAll<string, int>(jobArguments[2].Split(';'), int.Parse));
            }
        }

        public static void Main(string[] args)
        {
            Program p = new Program();
            Dictionary<int, Job[]> groupedJobs = Job.SortJobsInGroups(p.jobs);

            foreach (int groupID in groupedJobs.Keys)
            {
                Console.WriteLine("[Group " + groupID + "]");
                foreach (Job job in groupedJobs[groupID])
                {
                    Console.WriteLine(job.jobID + " - " + job.jobName);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
